#!/bin/bash

red=`tput setaf 1`
green=`tput setaf 2`
cyan=`tput setaf 6`
reset=`tput sgr0`

LINE="${cyan}|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||${reset}"

echo $LINE
echo "${cyan}||||||||||||||||||||||||||| Début des tests |||||||||||||||||||||||||||${reset}"
echo $LINE

echo "${green}Installation des dépendences de tests${reset}"
pip3 install mysql-connector

echo "${green}Exécution des tests unitaires${reset}"
python3 -m unittest discover -s project -v -p test_*.py
echo $LINE

echo "${green}Exécution du script deployLocal.sh${reset}"
bash ./deployLocal.sh >> /dev/null
echo $LINE

echo "${green}Attente du démarrage de l'application${reset}"
sleep 2
echo $LINE

echo "${green}Exécution des tests de déploiement${reset}"
python3 -m unittest discover -s project -v -p dtest_*.py

echo $LINE
echo "${cyan}|||||||||||||||||||||||||||| Fin des tests ||||||||||||||||||||||||||||${reset}"
echo $LINE