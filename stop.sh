#!/bin/bash

red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

echo "${red}Arrêt des conteneurs${reset}"
docker-compose -f project/docker/docker-compose.yml --env-file project/docker/local.env --project-directory . down