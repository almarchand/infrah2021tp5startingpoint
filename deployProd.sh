#!/bin/bash

red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

echo "${red}Arrêt du conteneur${reset}"
docker stop tp5_amh_jr

echo "${red}Suppression du conteneur${reset}"
docker rm tp5_amh_jr

echo "${red}Suppression de l'image${reset}"
docker rmi tp5_amh_jr_image

echo "${red}Suppression du volume${reset}"
docker volume remove tp5_amh_jr_vol

echo "${green}Création du volume${reset}"
docker volume create --name tp5_amh_jr_vol --opt device=$PWD --opt o=bind --opt type=none

echo "${green}Génération de l'image${reset}"
docker build -t tp5_amh_jr_image -f ./project/docker/DockerfileAPI .

echo "${green}Démarrage du conteneur accessible au port externe ${red}5555${reset}"
docker run -p 5555:5555 --env-file ./project/docker/.env -d --mount source=tp5_amh_jr_vol,target=/mnt/app/ --name tp5_amh_jr tp5_amh_jr_image
